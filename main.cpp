#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <fstream>
#include <string>

#include "huffman.h"

using namespace std;

int main(int argc, char **argv) {
	bool compress = true;
	string filename, outname;

	for (int i = 1; i < argc; ++i) {
		string arg(argv[i]);

		if (arg == "-c") {
			compress = true;
		} else if (arg == "-e") {
			compress = false;
		} else if (arg == "-o") {
			if (++i < argc) {
				outname = argv[i];
			} else {
				cout << "-o must have value (ie -o unhuffed)" << endl;
				return 1;
			}
		} else if (arg == "-h") {
			cout << "Usage: huffman [-c|-e] -o OUTPUT FILENAME" << endl;
			cout << endl;
			cout << " Arguments:" << endl;
			cout << "  -c               compress file (default)" << endl;
			cout << "  -e               extract file" << endl;
			cout << "  -o filename      set output filename" << endl;
			cout << endl;
			return 0;
		} else {
			filename = arg;
		}
	}

	if (filename == "") {
		cout << "Input file not specified" << endl;
		return 1;
	}
	
	if (outname == "") {
		cout << "Output file not specified" << endl;
		return 1;
	}

	ifstream input(filename.c_str(), ios::binary);

	if (input.fail()) {
		cout << "File not found" << endl;
		return 1;
	}
	
	ofstream output(outname.c_str(), ios::binary);
	
	if (output.fail()) {
		cout << "Couldn't create output file" << endl;
		return 1;
	}

	try {		
		if (compress) {
			zulyaev::huffman::Coder coder;

			coder.formTable(input, 8);
			coder.writeTable(output);

			input.clear();
			input.seekg(0, ios::beg);

			coder.compress(input, output);
		} else {
			zulyaev::huffman::Decoder decoder;

			decoder.readTable(input);
			decoder.decompress(input, output);
		}
	} catch(exception& ex) {
		cout << ex.what() << endl;
	}
}
