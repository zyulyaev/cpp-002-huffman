#include "huffman.h"

#include <string>
#include <sstream>

#define BOOST_TEST_MODULE
#include <boost/test/included/unit_test.hpp>

using namespace zulyaev;
using namespace std;

#define COUNT 10000

void fill(bool* a, util::bits& b, size_t length = COUNT) {
	for (size_t i = 0; i < length; ++i) {
		a[i] = rand() % 2;
		if (b.length() > i) {
			b.set(i, a[i]);
		} else {
			b.append(a[i]);
		}
	}
}

void equal(const bool* a, const util::bits& b, size_t from = 0, size_t to = COUNT) {
	BOOST_CHECK_EQUAL(b.length(), to - from);
	for (size_t i = from; i < to; ++i) {
		BOOST_CHECK_EQUAL(a[i], b[i - from]);
		BOOST_CHECK_EQUAL(a[i], b.at(i - from));
	}
}

BOOST_AUTO_TEST_CASE(bits_fill) {
	bool a[COUNT];
	util::bits b;

	fill(a, b);
	equal(a, b);
}

BOOST_AUTO_TEST_CASE(bits_set) {
	bool a[COUNT];
	util::bits b;

	fill(a, b);

	for (size_t _ = 0; _ < COUNT; ++_) {
		size_t i = rand() % COUNT;
		a[i] = rand() % 2;
		b.set(i, a[i]);
	}

	equal(a, b);
}

BOOST_AUTO_TEST_CASE(bits_shift) {
	bool a[COUNT];
	util::bits b;
	
	fill(a, b);
	size_t offset = rand() % COUNT;

	b.shift(offset);
	equal(a, b, offset);
}

BOOST_AUTO_TEST_CASE(bits_sub) {
	bool a[COUNT];
	util::bits b;

	fill(a, b);
	size_t from = rand() % COUNT;
	size_t to = rand() % (COUNT - from + 1) + from;

	util::bits* sub = b.sub(from, to - from);

	equal(a, *sub, from, to);

	delete sub;
}

BOOST_AUTO_TEST_CASE(bits_reverse) {
	bool a[COUNT];
	util::bits b;

	fill(a, b);

	std::reverse(a, a + COUNT);
	
	util::bits* r = b.reverse();
	
	equal(a, *r);

	delete r;
}

BOOST_AUTO_TEST_CASE(bits_operator_equal) {
	util::bits a, b;

	for (size_t i = 0; i < COUNT; ++i) {
		bool val = rand() % 2;

		a += val;
		b += val;
	}

	BOOST_CHECK_EQUAL(a == b, true);

	size_t i = rand() % COUNT;
	a.set(i, !a[i]);

	BOOST_CHECK_EQUAL(a == b, false);
	a.set(i, !a[i]);

	BOOST_CHECK_EQUAL(a == b, true);

	b.shift(1);

	BOOST_CHECK_EQUAL(a == b, false);
}

BOOST_AUTO_TEST_CASE(bits_all) {
	bool a[2 * COUNT];
	util::bits b;

	fill(a, b);

	size_t offset = rand() % COUNT;
	b.shift(offset);
	
	size_t overhead = rand() % COUNT;

	for (size_t i = 0; i < overhead; ++i) {
		bool val = rand() % 2;
		a[i + COUNT] = val;
		b += val;
	}

	equal(a, b, offset, COUNT + overhead);
}

BOOST_AUTO_TEST_CASE(bits_exceptions) {
	bool a[COUNT];
	util::bits b;

	fill(a, b);

	BOOST_CHECK_THROW(b.shift(COUNT + 1), util::IllegalArgumentException);
	BOOST_CHECK_THROW(b[-1], util::IndexOutOfBoundsException);
	BOOST_CHECK_THROW(b.at(COUNT), util::IndexOutOfBoundsException);
	BOOST_CHECK_THROW(b.set(COUNT, true), util::IndexOutOfBoundsException);
	BOOST_CHECK_THROW(b.sub(rand() % COUNT, COUNT), util::IndexOutOfBoundsException);
	BOOST_CHECK_THROW(b.sub(COUNT, 1), util::IndexOutOfBoundsException);
}

void testCompressDecompress(const std::string & data, uint8_t wordLength) {
	huffman::Coder coder;
	huffman::Decoder decoder;

	istringstream orig(data, ios::binary);
	ostringstream packedOut(ios::binary);

	coder.formTable(orig, wordLength);
	coder.writeTable(packedOut);

	orig.clear();
	orig.seekg(0, ios::beg);
	
	coder.compress(orig, packedOut);

	istringstream packedIn(packedOut.str(), ios::binary);
	ostringstream unpacked(ios::binary);

	decoder.readTable(packedIn);
	decoder.decompress(packedIn, unpacked);

	BOOST_CHECK_EQUAL(data, unpacked.str());
}

BOOST_AUTO_TEST_CASE(compress_decompress) {
	vector<string> str;
	str.push_back("Hello World!");
	str.push_back("");
	str.push_back("1234567890qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>!@#$%^&*()_+-=!№;%:?*(0ЙЦУКЕНГШЩЗЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,йцукенгшщзхъфывапролджэячсмитьбю.|");
	str.push_back("1111111111111111111111111111111111111111111111111111111111111222222a");
	str.push_back("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed libero nisi, vel mollis nisl. Suspendisse potenti. Donec suscipit viverra justo, sit amet placerat elit posuere at. Phasellus pharetra ultrices elit, ut laoreet libero viverra sed. Etiam enim nunc, congue non consequat vitae, posuere quis lorem. Etiam et purus non magna tempus mollis id nec diam. Donec interdum venenatis libero, in congue diam commodo id. Curabitur ut nisl tortor. Duis sed libero ut erat ultricies blandit ac at sem. Nulla urna velit, mollis non imperdiet vitae, molestie sed lacus. Curabitur ullamcorper mollis erat, sed rutrum nisl sollicitudin id. Nunc quis tempor justo. Vestibulum bibendum ullamcorper imperdiet. Maecenas consectetur consequat erat, eget eleifend arcu luctus et. Donec tincidunt iaculis lorem, eget placerat mauris vestibulum ut. Proin ut leo nibh, ac placerat libero. Integer metus lacus, luctus nec tincidunt posuere, pharetra sit amet mauris. Aliquam erat volutpat. Cras accumsan lacinia ipsum sit amet hendrerit. Suspendisse potenti. Morbi sed nisi et mi lacinia euismod. In in eros ut turpis ultrices eleifend. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas rhoncus nunc sed neque pellentesque et iaculis sapien lacinia. Cras in odio a mauris lacinia porttitor. Etiam turpis urna, suscipit non rhoncus eget, porttitor sed dui. Nunc quis elit sed velit mollis venenatis. Pellentesque vel magna turpis, non dignissim dui. Phasellus sed odio ut metus ornare bibendum. Mauris id mi sapien. Cras malesuada, lorem et sodales feugiat, lacus ipsum eleifend elit, vitae pharetra nisl massa eget augue. Fusce consequat placerat arcu, at mollis turpis molestie eu. Integer commodo, massa id ultricies vestibulum, quam justo euismod sapien, non porttitor enim lacus a urna. Fusce interdum dolor quis diam semper convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse nec tortor non augue mollis fringilla. Aenean et enim turpis. Quisque at nisi erat, consectetur tristique dui. Phasellus vel massa tortor. Proin est lacus, tristique in viverra eget, fringilla vel ipsum. Integer ornare velit at turpis ornare et lacinia libero blandit. Sed nec erat eget odio tincidunt commodo nec a elit. Ut urna eros, semper non auctor pharetra, ultrices eleifend velit. Duis ut velit lacinia lorem pharetra condimentum eget non libero. Praesent facilisis vestibulum erat sit amet accumsan. Ut felis risus, ultricies at eleifend id, gravida at odio. Etiam magna ipsum, egestas vitae rhoncus sit amet, porta vel sem. Curabitur semper condimentum lectus, eget laoreet enim eleifend sit amet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ac nunc at magna commodo ultricies. In felis dolor, convallis eu elementum eu, bibendum vel tortor. Quisque mattis auctor magna, in condimentum purus sollicitudin sed. Praesent non ante leo. Donec ultricies convallis cursus.");
	str.push_back("\t\n\r");
	
	for (int i = 1; i < 17; ++i) {
		for (size_t j = 0; j < str.size(); ++j) {
			testCompressDecompress(str[j], i);
		}
	}

	BOOST_CHECK_THROW(testCompressDecompress("Hello world!", 0), util::IllegalArgumentException);
	BOOST_CHECK_THROW(testCompressDecompress("Hello world!", 17), util::IllegalArgumentException);
}
