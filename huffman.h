#include <iostream>
#include <exception>
#include <vector>
#include <deque>
#include <map>

#include <stdint.h>

namespace zulyaev {
	namespace util {
		class IndexOutOfBoundsException : public std::exception {
		private:
			std::string message;
		public:
			IndexOutOfBoundsException(size_t index, size_t bound);
			IndexOutOfBoundsException(size_t offset, size_t length, size_t bound);
			virtual ~IndexOutOfBoundsException() throw();
			virtual const char* what() const throw();
		};
		
		class IllegalArgumentException : public std::exception {
		private:
			std::string message;
		public:
			IllegalArgumentException(const std::string & message);
			virtual ~IllegalArgumentException() throw();
			virtual const char* what() const throw();
		};

		class IllegalStateException : public std::exception {
		private:
			std::string message;
		public:
			IllegalStateException(const std::string & message);
			virtual ~IllegalStateException() throw();
			virtual const char* what() const throw();
		};

		class bits {
		private:
			std::deque<int> _value_;
			size_t _length_, _offset_;

			void checkIndex(size_t i) const;

		public:
			bits();
			bits(const char* data, size_t count);
			bits(const bits& b);
			virtual ~bits();

			size_t length() const;

			bits& append(bool bit);
			bits& append(const bits& b);

			bits& set(size_t i, bool bit);
			bool at(size_t i) const;
			bits& shift(size_t bits);

			bits* sub(size_t offset, size_t length = -1);
			bits* reverse() const;

			bits& operator+=(bool bit);
			bits& operator+=(const bits& b);
			bool operator[](size_t i) const;

			bool operator<(const bits& c) const;
			bool operator==(const bits& c) const;
			bits& operator=(const bits& b);
		};

		class BufferedBitReader {
		private:
			std::istream& _stream_;
			bits _buffer_;
		public:
			BufferedBitReader(std::istream& stream);
			bits* read(size_t bits);
			bool eof() const;
		};

		class BufferedBitWriter {
		private:
			std::ostream& _stream_;
			bits _buffer_;

			char cutFirstChar();
		public:
			BufferedBitWriter(std::ostream& stream);
			virtual ~BufferedBitWriter();
			void write(const bits& data);
			void flush();
		};
	}
	namespace huffman {
		typedef std::map<util::bits, util::bits*> table_type;

		class NoSuchEntryInTableException : public std::exception {
		private:
			std::string message;
		public:
			NoSuchEntryInTableException(const util::bits& entry);
			virtual ~NoSuchEntryInTableException() throw();
			virtual const char* what() const throw();
		};

		class Coder {
		private:
			class node {
			public:
				std::vector<util::bits> list;
				size_t weight;
				node(const util::bits& bits, size_t weight);
				node(const node& a, const node& b);

				bool operator<(const node& n) const;
			};

			table_type _table_;
			uint8_t _wordLength_;
		public:
			Coder();
			Coder(const Coder& c);
			virtual ~Coder();

			void formTable(std::istream& input, uint8_t wordLength);
			void writeTable(std::ostream& output) const;
			void compress(std::istream& input, std::ostream& output) const;
		};

		class Decoder {
		private:
			table_type _table_;
			size_t _maxCodedLength_;
		public:
			Decoder();
			Decoder(const Decoder& d);
			virtual ~Decoder();

			void readTable(std::istream& input);
			void decompress(std::istream& input, std::ostream& output) const;
		};
	}
}
