LIB_NAME=huffman
APP_EXEC=huffman
TEST_EXEC=test

CC=g++
CFLAGS=-c -Wall -O2
SFLAGS=-Wall -shared -fpic
LFLAGS=-L. -l$(LIB_NAME)
LFLAGS=huffman.o

LIB_FILE=lib$(LIB_NAME).so

all: test app

test: test.o huffman.so
	$(CC) test.o -o $(TEST_EXEC) $(LFLAGS)

app: main.o huffman.so
	$(CC) main.o -o $(APP_EXEC) $(LFLAGS)

test.o: test.cpp
	$(CC) $(CFLAGS) test.cpp

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

huffman.so: huffman.cpp
	$(CC) $(SFLAGS) -o $(LIB_FILE) huffman.cpp
	$(CC) $(CFLAGS) huffman.cpp

clean:
	rm -f *.o $(APP_EXEC) $(TEST_EXEC) $(LIB_FILE)
