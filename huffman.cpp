#include "huffman.h"

#include <algorithm>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define CHAR_BIT (sizeof(char)*8)
#define CELL_BIT (sizeof(int)*8)

namespace zulyaev {
	namespace util {

		IndexOutOfBoundsException::IndexOutOfBoundsException(size_t index, size_t bound) {
			std::stringstream stream;
			stream << "Index: " << index << ", size: " << bound;
			message = stream.str();
		}

		IndexOutOfBoundsException::IndexOutOfBoundsException(size_t offset, size_t length, size_t bound) {
			std::stringstream stream;
			stream << "From: " << offset << ", to: " << offset + length - 1 << ", size: " << bound;
			message = stream.str();
		}

		IndexOutOfBoundsException::~IndexOutOfBoundsException() throw() {}

		const char* IndexOutOfBoundsException::what() const throw() {
			return message.c_str();
		}

		IllegalArgumentException::IllegalArgumentException(const std::string & message) : message(message) {}

		IllegalArgumentException::~IllegalArgumentException() throw() {}

		const char* IllegalArgumentException::what() const throw() {
			return message.c_str();
		}

		IllegalStateException::IllegalStateException(const std::string & message) : message(message) {}

		IllegalStateException::~IllegalStateException() throw() {}

		const char* IllegalStateException::what() const throw() {
			return message.c_str();
		}

		bits::bits() : _length_(0), _offset_(0) {
		}

		bits::bits(const char* data, size_t count) : _length_(0), _offset_(0) {
			for (size_t i = 0; i < count; ++i) {
				append(data[i / CHAR_BIT] & (1 << i % CHAR_BIT));
			}
		}

		bits::bits(const bits& b) :
			_value_(b._value_), _length_(b._length_), _offset_(b._offset_) {
		}

		bits::~bits() {
		}

		void bits::checkIndex(size_t i) const {
			if (i >= _length_) {
				throw IndexOutOfBoundsException(i, _length_);
			}
		}

		size_t bits::length() const {
			return _length_;
		}

		bits& bits::append(bool bit) {
			if ((_length_ + _offset_) % CELL_BIT == 0) {
				_value_.push_back(0);
			}

			if (bit) {
				_value_.back() |= 1 << (_length_ + _offset_) % CELL_BIT;
			}

			++_length_;

			return *this;
		}

		bits& bits::append(const bits& b) {
			for (size_t i = 0; i < b.length(); ++i) {
				append(b[i]);
			}

			return *this;
		}

		bits& bits::set(size_t i, bool bit) {
			checkIndex(i);

			i += _offset_;

			if (bit) {
				_value_[i / CELL_BIT] |= 1 << i % CELL_BIT;
			} else {
				_value_[i / CELL_BIT] &= ~(1 << i % CELL_BIT);
			}

			return *this;
		}

		bool bits::at(size_t i) const {
			checkIndex(i);

			i += _offset_;

			return _value_[i / CELL_BIT] & (1 << i % CELL_BIT);
		}

		bits& bits::shift(size_t length) {
			if (length > _length_) {
				throw IllegalArgumentException("Can't shift more than length of bit array.");
			}
			
			_offset_ += length;
			_length_ -= length;

			while (_offset_ >= CELL_BIT) {
				_value_.pop_front();
				_offset_ -= CELL_BIT;
			}

			return *this;
		}

		bits* bits::sub(size_t offset, size_t len) {
			if (offset > _length_ || offset + len > _length_) {
				throw IndexOutOfBoundsException(offset, len);
			}

			bits* result = new bits();

			for (size_t i = 0; i < len; ++i) {
				result->append(at(i + offset));
			}

			return result;
		}

		bits* bits::reverse() const {
			bits* result = new bits();
			for (size_t i = 0; i < _length_; ++i) {
				result->append(at(_length_ - i - 1));
			}
			return result;
		}

		bits& bits::operator+=(bool bit) {
			return append(bit);
		}

		bits& bits::operator+=(const bits& b) {
			return append(b);
		}

		bool bits::operator[](size_t i) const {
			return at(i);
		}

		bool bits::operator<(const bits& b) const {
			for (size_t i = 0, len = std::min(_length_, b.length()); i < len; ++i) {
				if (at(i) != b[i]) {
					return at(i) < b[i];
				}
			}
			return _length_ < b.length();
		}

		bool bits::operator==(const bits& b) const {
			if (_length_ != b._length_) {
				return false;
			}
			for (size_t i = 0; i < _length_; ++i) {
				if (at(i) != b[i]) {
					return false;
				}
			}
			return true;
		}

		bits& bits::operator=(const bits& b) {
			_value_ = b._value_;
			_length_ = b._length_;
			_offset_ = b._offset_;

			return *this;
		}

		BufferedBitReader::BufferedBitReader(std::istream& stream) : _stream_(stream) {
		}

		bits* BufferedBitReader::read(size_t length) {
			char c;

			while (_buffer_.length() < length) {
				_stream_.get(c);

				if (!_stream_.good()) {
					break;
				}

				for (size_t i = 0; i < CHAR_BIT; ++i) {
					_buffer_ += c & (1 << i);
				}
			}

			size_t len = std::min(_buffer_.length(), length);

			bits* result = _buffer_.sub(0, len);
			_buffer_.shift(len);

			return result;
		}

		bool BufferedBitReader::eof() const {
			return _buffer_.length() == 0 && _stream_.eof();
		}

		BufferedBitWriter::BufferedBitWriter(std::ostream& stream) : _stream_(stream) {
		}

		BufferedBitWriter::~BufferedBitWriter() {
			flush();
		}

		char BufferedBitWriter::cutFirstChar() {
			char c = 0;
			size_t len = std::min(CHAR_BIT, _buffer_.length());
			for (size_t i = 0; i < len; ++i) {
				c |= _buffer_[i] << i;
			}
			_buffer_.shift(len);
			return c;
		}

		void BufferedBitWriter::write(const bits& data) {
			_buffer_ += data;

			while (_buffer_.length() >= CHAR_BIT) {
				_stream_.put(cutFirstChar());
			}
		}

		void BufferedBitWriter::flush() {
			while (_buffer_.length()) {
				_stream_.put(cutFirstChar());
			}
			_stream_.flush();
		}
	}

	namespace huffman {

		NoSuchEntryInTableException::NoSuchEntryInTableException(const util::bits& entry) {
			message = "Entry '";
			for (size_t i = 0; i < entry.length(); ++i) {
				message += entry[i] ? "1" : "0";
			}
			message += "' not found";
		}

		NoSuchEntryInTableException::~NoSuchEntryInTableException() throw() {}

		const char* NoSuchEntryInTableException::what() const throw() {
			return message.c_str();
		}

		Coder::Coder() : _wordLength_(0) {
		}

		Coder::Coder(const Coder& c) : _wordLength_(c._wordLength_) {
			for (table_type::const_iterator it = c._table_.begin(); it != c._table_.end(); ++it) {
				_table_[it->first] = new util::bits(*it->second);
			}
		}

		Coder::~Coder() {
			for (table_type::iterator it = _table_.begin(); it != _table_.end(); ++it) {
				delete it->second;
			}
		}

		Coder::node::node(const util::bits& bits, size_t weight) : weight(weight) {
			list.push_back(bits);
		}

		Coder::node::node(const Coder::node& a, const Coder::node& b) {
			list.insert(list.end(), a.list.begin(), a.list.end());
			list.insert(list.end(), b.list.begin(), b.list.end());
			weight = a.weight + b.weight;
		}

		bool Coder::node::operator<(const Coder::node& n) const {
			return weight < n.weight;
		}

		void Coder::formTable(std::istream& input, uint8_t wordLength) {
			if (wordLength == 0 || wordLength > 16) {
				throw util::IllegalArgumentException("Word length must be positive and <= 16");
			}
			
			_wordLength_ = wordLength;

			std::multiset<node> nodes;
			std::map<util::bits, size_t> weights;
			util::BufferedBitReader reader(input);

			util::bits* read = 0;
			do {
				if (read) delete read;
				read = reader.read(_wordLength_);
				if (read->length() == 0) {
					break;
				}
				weights[*read]++;
			} while (read->length() == _wordLength_);

			if (read) delete read;

			// add finalizer to table
			weights[util::bits()]++;

			for (std::map<util::bits, size_t>::iterator it = weights.begin(); it != weights.end(); ++it) {
				nodes.insert(node(it->first, it->second));
			}

			// clear table before filling 
			_table_.clear();

			if (nodes.size() == 1) {
				_table_[util::bits()] = new util::bits("\0", 1);
			}

			while (nodes.size() > 1) {
				std::set<node>::iterator it = nodes.begin();
				node a = *it;
				nodes.erase(it);

				it = nodes.begin();
				node b = *it;
				nodes.erase(it);

				for (size_t i = 0; i < a.list.size(); ++i) {
					table_type::iterator it = _table_.find(a.list[i]);
					if (it != _table_.end()) {
						it->second->append(0);
					} else {
						_table_[a.list[i]] = new util::bits("\0", 1);
					}
				}

				for (size_t i = 0; i < b.list.size(); ++i) {
					table_type::iterator it = _table_.find(b.list[i]);
					if (it != _table_.end()) {
						it->second->append(1);
					} else {
						_table_[b.list[i]] = new util::bits("\1", 1);
					}
				}

				nodes.insert(node(a, b));
			}

			for (table_type::iterator it = _table_.begin(); it != _table_.end(); ++it) {
				util::bits* b = it->second;
				it->second = b->reverse();
				delete b;
			}
		}

		void Coder::writeTable(std::ostream& output) const {
			size_t size = _table_.size();
			output.write((char*)&size, sizeof(size));

			util::BufferedBitWriter writer(output);
			int hash = 0, cnt = 0;
			for (table_type::const_iterator it = _table_.begin(); it != _table_.end(); ++it, ++cnt) {
				char fromLength = it->first.length();
				uint16_t toLength = it->second->length();

				writer.write(util::bits(&fromLength, CHAR_BIT));
				writer.write(util::bits((char*)&toLength, sizeof(toLength) * 8));

				writer.write(it->first);
				writer.write(*it->second);

				for (int i = 0; i < fromLength; ++i) {
					hash *= 31;
					hash += it->first[i];
				}

				for (int i = 0; i < toLength; ++i) {
					hash *= 31;
					hash += it->second->at(i);
				}				

				if (cnt && cnt % 100 == 0) {
					writer.write(util::bits((char*)&hash, sizeof(hash)*8));
				}
			}
			writer.write(util::bits((char*)&hash, sizeof(hash)*8));

			writer.flush();
		}

		void Coder::compress(std::istream& input, std::ostream& output) const {			
			if (_wordLength_ == 0) {
				throw util::IllegalStateException("Table not initialized");
			}
		
			util::BufferedBitReader reader(input);
			util::BufferedBitWriter writer(output);

			util::bits* read = 0;
			do {
				if (read) delete read;
				read = reader.read(_wordLength_);
				if (read->length() == 0) {
					break;
				}
				if (_table_.find(*read) == _table_.end()) {
					util::bits block = *read;
					delete read;
					throw NoSuchEntryInTableException(block);
				}
				writer.write(*_table_.at(*read));
			} while (read->length() == _wordLength_);
			if (read) delete read;

			// write finalizer
			writer.write(*_table_.at(util::bits()));

			writer.flush();
		}

		Decoder::Decoder() : _maxCodedLength_(0) {}

		Decoder::Decoder(const Decoder& d) : _maxCodedLength_(d._maxCodedLength_) {
			for (table_type::const_iterator it = d._table_.begin(); it != d._table_.end(); ++it) {
				_table_[it->first] = new util::bits(*it->second);
			}
		}

		Decoder::~Decoder() {
			for (table_type::iterator it = _table_.begin(); it != _table_.end(); ++it) {
				delete it->second;
			}
		}

		void Decoder::readTable(std::istream& input) {
			size_t tableSize;
			input.read((char*)&tableSize, sizeof(tableSize));

			util::BufferedBitReader reader(input);

			int hash = 0;
			for (int cnt = 0; tableSize; --tableSize, cnt++) {
				util::bits* toLengthBits = reader.read(CHAR_BIT);
				util::bits* fromLengthBits = reader.read(sizeof(uint16_t) * 8);

				char toLength = 0;
				uint16_t fromLength = 0;

				for (size_t i = 0; i < toLengthBits->length(); ++i) {
					toLength |= toLengthBits->at(i) << i;
				}

				for (size_t i = 0; i < fromLengthBits->length(); ++i) {
					fromLength |= fromLengthBits->at(i) << i;
				}

				delete toLengthBits;
				delete fromLengthBits;

				util::bits* to = reader.read(toLength);
				util::bits* from = reader.read(fromLength);

				for (size_t i = 0; i < to->length(); ++i) {
					hash *= 31;
					hash += to->at(i);
				}

				for (size_t i = 0; i < from->length(); ++i) {
					hash *= 31;
					hash += from->at(i);
				}

				table_type::iterator it = _table_.find(*from);
				if (it != _table_.end()) {
					delete it->second;
					it->second = to;
				} else {
					_table_[*from] = to;
				}

				if (_maxCodedLength_ < from->length()) {
					_maxCodedLength_ = from->length();
				}

				delete from;

				if (cnt && cnt % 100 == 0) {
					util::bits* h = reader.read(sizeof(hash) * 8);
					int control = 0;
					for (size_t i = 0; i < h->length(); ++i) {
						control |= h->at(i) << i;
					}
					
					delete h;

					if (control != hash) {
						throw util::IllegalArgumentException("Input doesn't have valid huffman table");
					}
				}
			}
			
			util::bits* h = reader.read(sizeof(hash) * 8);
			int control = 0;
			for (size_t i = 0; i < h->length(); ++i) {
				control |= h->at(i) << i;
			}

			delete h;
					
			if (control != hash) {
				throw util::IllegalArgumentException("Input doesn't have valid huffman table");
			}
		}

		void Decoder::decompress(std::istream& input, std::ostream& output) const {
			util::BufferedBitReader reader(input);
			util::BufferedBitWriter writer(output);

			while (!reader.eof()) {
				util::bits chunk;

				while (_table_.find(chunk) == _table_.end() && chunk.length() < _maxCodedLength_) {
					util::bits* read = reader.read(1);
					if (read->length() == 0) {
						delete read;
						break;
					}
					chunk += *read;
					delete read;
				}

				if (chunk.length() == 0) {
					break;
				}

				if (_table_.find(chunk) == _table_.end()) {
					throw NoSuchEntryInTableException(chunk);
				}

				util::bits* decompressed = _table_.at(chunk);

				if (decompressed->length() == 0) {
					break;
				}

				writer.write(*decompressed);
			}

			writer.flush();
		}
	}
}
